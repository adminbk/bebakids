﻿namespace BebaKids
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnBarkod = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrijemnica = new System.Windows.Forms.Button();
            this.btnPrenosnica = new System.Windows.Forms.Button();
            this.btnMagacin = new System.Windows.Forms.Button();
            this.btnPrijavaRadnika = new System.Windows.Forms.Button();
            this.btnPrijavaOdsustva = new System.Windows.Forms.Button();
            this.btnPrijavaPopisa = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRealizacijaCekova = new System.Windows.Forms.Button();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.popisObjektaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosPopisaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrenosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledDnevnogIzveštajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajFransizePoDanuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajLageraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proizvodnjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prenosSifaraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalkulacijaArtiklaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.proveraCeneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevniPrometToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosPrometaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loyaltiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosNovogClanaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosNovogClanaCGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.racunovodstvoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.osvezavanjeIzvodaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.btnProveraFakture = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBarkod
            // 
            this.btnBarkod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBarkod.Location = new System.Drawing.Point(313, 44);
            this.btnBarkod.Name = "btnBarkod";
            this.btnBarkod.Size = new System.Drawing.Size(136, 31);
            this.btnBarkod.TabIndex = 0;
            this.btnBarkod.Text = "Unos Barkodova";
            this.btnBarkod.UseVisualStyleBackColor = true;
            this.btnBarkod.Click += new System.EventHandler(this.btnBarkodovi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(32, 209);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Objekat: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(97, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 4;
            // 
            // btnPrijemnica
            // 
            this.btnPrijemnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrijemnica.Location = new System.Drawing.Point(313, 83);
            this.btnPrijemnica.Name = "btnPrijemnica";
            this.btnPrijemnica.Size = new System.Drawing.Size(136, 31);
            this.btnPrijemnica.TabIndex = 1;
            this.btnPrijemnica.Text = "Provera Prijemnice";
            this.btnPrijemnica.UseVisualStyleBackColor = true;
            this.btnPrijemnica.Click += new System.EventHandler(this.btnOtpremnica_Click);
            // 
            // btnPrenosnica
            // 
            this.btnPrenosnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrenosnica.Location = new System.Drawing.Point(314, 119);
            this.btnPrenosnica.Name = "btnPrenosnica";
            this.btnPrenosnica.Size = new System.Drawing.Size(136, 31);
            this.btnPrenosnica.TabIndex = 2;
            this.btnPrenosnica.Text = "Provera Prenosnice";
            this.btnPrenosnica.UseVisualStyleBackColor = true;
            this.btnPrenosnica.Click += new System.EventHandler(this.btnPrijemnica_Click);
            // 
            // btnMagacin
            // 
            this.btnMagacin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMagacin.Location = new System.Drawing.Point(149, 157);
            this.btnMagacin.Name = "btnMagacin";
            this.btnMagacin.Size = new System.Drawing.Size(159, 31);
            this.btnMagacin.TabIndex = 3;
            this.btnMagacin.Text = "Provera Prenosnice MAG";
            this.btnMagacin.UseVisualStyleBackColor = true;
            this.btnMagacin.Click += new System.EventHandler(this.btnMagacin_Click);
            // 
            // btnPrijavaRadnika
            // 
            this.btnPrijavaRadnika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrijavaRadnika.Location = new System.Drawing.Point(462, 44);
            this.btnPrijavaRadnika.Name = "btnPrijavaRadnika";
            this.btnPrijavaRadnika.Size = new System.Drawing.Size(136, 31);
            this.btnPrijavaRadnika.TabIndex = 0;
            this.btnPrijavaRadnika.Text = "Prijava radnika";
            this.btnPrijavaRadnika.UseVisualStyleBackColor = true;
            this.btnPrijavaRadnika.Click += new System.EventHandler(this.btnPrijvaRadnika_Click);
            // 
            // btnPrijavaOdsustva
            // 
            this.btnPrijavaOdsustva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrijavaOdsustva.Location = new System.Drawing.Point(462, 83);
            this.btnPrijavaOdsustva.Name = "btnPrijavaOdsustva";
            this.btnPrijavaOdsustva.Size = new System.Drawing.Size(136, 31);
            this.btnPrijavaOdsustva.TabIndex = 1;
            this.btnPrijavaOdsustva.Text = "Prijava Odsustva";
            this.btnPrijavaOdsustva.UseVisualStyleBackColor = true;
            this.btnPrijavaOdsustva.Click += new System.EventHandler(this.btnPrijavaOdsustva_Click);
            // 
            // btnPrijavaPopisa
            // 
            this.btnPrijavaPopisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrijavaPopisa.Location = new System.Drawing.Point(463, 119);
            this.btnPrijavaPopisa.Name = "btnPrijavaPopisa";
            this.btnPrijavaPopisa.Size = new System.Drawing.Size(136, 31);
            this.btnPrijavaPopisa.TabIndex = 2;
            this.btnPrijavaPopisa.Text = "Popis Objekta";
            this.btnPrijavaPopisa.UseVisualStyleBackColor = true;
            this.btnPrijavaPopisa.Click += new System.EventHandler(this.btnPopis_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(462, 157);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 31);
            this.button1.TabIndex = 2;
            this.button1.Text = "Dodatni sati";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label7.Location = new System.Drawing.Point(343, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(256, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Copyright @ Kids Beba doo, Created by Marko Vesic";
            // 
            // btnRealizacijaCekova
            // 
            this.btnRealizacijaCekova.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnRealizacijaCekova.Location = new System.Drawing.Point(314, 156);
            this.btnRealizacijaCekova.Name = "btnRealizacijaCekova";
            this.btnRealizacijaCekova.Size = new System.Drawing.Size(136, 31);
            this.btnRealizacijaCekova.TabIndex = 2;
            this.btnRealizacijaCekova.Text = "Realizacija Cekova";
            this.btnRealizacijaCekova.UseVisualStyleBackColor = true;
            this.btnRealizacijaCekova.Click += new System.EventHandler(this.btnRealizacijaCekova_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.popisObjektaToolStripMenuItem,
            this.izvestajiToolStripMenuItem,
            this.proizvodnjaToolStripMenuItem,
            this.dnevniPrometToolStripMenuItem,
            this.loyaltiToolStripMenuItem,
            this.racunovodstvoToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(641, 24);
            this.menuStrip2.TabIndex = 10;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // popisObjektaToolStripMenuItem
            // 
            this.popisObjektaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unosPopisaToolStripMenuItem,
            this.pregledPrenosToolStripMenuItem,
            this.radniciToolStripMenuItem,
            this.pregledDnevnogIzveštajaToolStripMenuItem});
            this.popisObjektaToolStripMenuItem.Name = "popisObjektaToolStripMenuItem";
            this.popisObjektaToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.popisObjektaToolStripMenuItem.Text = "Aktivnost";
            // 
            // unosPopisaToolStripMenuItem
            // 
            this.unosPopisaToolStripMenuItem.Name = "unosPopisaToolStripMenuItem";
            this.unosPopisaToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.unosPopisaToolStripMenuItem.Text = "Unos popisa";
            this.unosPopisaToolStripMenuItem.Click += new System.EventHandler(this.unosPopisaToolStripMenuItem_Click);
            // 
            // pregledPrenosToolStripMenuItem
            // 
            this.pregledPrenosToolStripMenuItem.Name = "pregledPrenosToolStripMenuItem";
            this.pregledPrenosToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.pregledPrenosToolStripMenuItem.Text = "Pregled prenos";
            this.pregledPrenosToolStripMenuItem.Click += new System.EventHandler(this.pregledPrenosToolStripMenuItem_Click);
            // 
            // radniciToolStripMenuItem
            // 
            this.radniciToolStripMenuItem.Name = "radniciToolStripMenuItem";
            this.radniciToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.radniciToolStripMenuItem.Text = "Radnici";
            this.radniciToolStripMenuItem.Click += new System.EventHandler(this.otvoriToolStripMenuItem_Click);
            // 
            // pregledDnevnogIzveštajaToolStripMenuItem
            // 
            this.pregledDnevnogIzveštajaToolStripMenuItem.Name = "pregledDnevnogIzveštajaToolStripMenuItem";
            this.pregledDnevnogIzveštajaToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.pregledDnevnogIzveštajaToolStripMenuItem.Text = "Pregled Dnevnog Izveštaja";
            this.pregledDnevnogIzveštajaToolStripMenuItem.Click += new System.EventHandler(this.pregledDnevnogIzveštajaToolStripMenuItem_Click);
            // 
            // izvestajiToolStripMenuItem
            // 
            this.izvestajiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izvestajFransizePoDanuToolStripMenuItem,
            this.izvestajLageraToolStripMenuItem});
            this.izvestajiToolStripMenuItem.Name = "izvestajiToolStripMenuItem";
            this.izvestajiToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.izvestajiToolStripMenuItem.Text = "Izvestaji";
            this.izvestajiToolStripMenuItem.Visible = false;
            // 
            // izvestajFransizePoDanuToolStripMenuItem
            // 
            this.izvestajFransizePoDanuToolStripMenuItem.Name = "izvestajFransizePoDanuToolStripMenuItem";
            this.izvestajFransizePoDanuToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.izvestajFransizePoDanuToolStripMenuItem.Text = "Izvestaj fransize po danu";
            this.izvestajFransizePoDanuToolStripMenuItem.Click += new System.EventHandler(this.izvestajFransizePoDanuToolStripMenuItem_Click);
            // 
            // izvestajLageraToolStripMenuItem
            // 
            this.izvestajLageraToolStripMenuItem.Name = "izvestajLageraToolStripMenuItem";
            this.izvestajLageraToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.izvestajLageraToolStripMenuItem.Text = "Izvestaj lagera";
            // 
            // proizvodnjaToolStripMenuItem
            // 
            this.proizvodnjaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prenosSifaraToolStripMenuItem,
            this.kalkulacijaArtiklaToolStripMenuItem,
            this.izvestajiToolStripMenuItem1,
            this.proveraCeneToolStripMenuItem});
            this.proizvodnjaToolStripMenuItem.Name = "proizvodnjaToolStripMenuItem";
            this.proizvodnjaToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.proizvodnjaToolStripMenuItem.Text = "Proizvodnja";
            this.proizvodnjaToolStripMenuItem.Visible = false;
            // 
            // prenosSifaraToolStripMenuItem
            // 
            this.prenosSifaraToolStripMenuItem.Name = "prenosSifaraToolStripMenuItem";
            this.prenosSifaraToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.prenosSifaraToolStripMenuItem.Text = "Prenos Sifara";
            this.prenosSifaraToolStripMenuItem.Click += new System.EventHandler(this.prenosSifaraToolStripMenuItem_Click);
            // 
            // kalkulacijaArtiklaToolStripMenuItem
            // 
            this.kalkulacijaArtiklaToolStripMenuItem.Name = "kalkulacijaArtiklaToolStripMenuItem";
            this.kalkulacijaArtiklaToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.kalkulacijaArtiklaToolStripMenuItem.Text = "Kalkulacija artikla";
            this.kalkulacijaArtiklaToolStripMenuItem.Click += new System.EventHandler(this.kalkulacijaArtiklaToolStripMenuItem_Click);
            // 
            // izvestajiToolStripMenuItem1
            // 
            this.izvestajiToolStripMenuItem1.Name = "izvestajiToolStripMenuItem1";
            this.izvestajiToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.izvestajiToolStripMenuItem1.Text = "Izvestaji";
            // 
            // proveraCeneToolStripMenuItem
            // 
            this.proveraCeneToolStripMenuItem.Name = "proveraCeneToolStripMenuItem";
            this.proveraCeneToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.proveraCeneToolStripMenuItem.Text = "Provera Cene";
            this.proveraCeneToolStripMenuItem.Click += new System.EventHandler(this.proveraCeneArtToolStripMenuItem_Click);
            // 
            // dnevniPrometToolStripMenuItem
            // 
            this.dnevniPrometToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unosPrometaToolStripMenuItem});
            this.dnevniPrometToolStripMenuItem.Name = "dnevniPrometToolStripMenuItem";
            this.dnevniPrometToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.dnevniPrometToolStripMenuItem.Text = "Dnevni promet";
            // 
            // unosPrometaToolStripMenuItem
            // 
            this.unosPrometaToolStripMenuItem.Name = "unosPrometaToolStripMenuItem";
            this.unosPrometaToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.unosPrometaToolStripMenuItem.Text = "Unos prometa";
            this.unosPrometaToolStripMenuItem.Click += new System.EventHandler(this.unosPrometaToolStripMenuItem_Click);
            // 
            // loyaltiToolStripMenuItem
            // 
            this.loyaltiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unosNovogClanaToolStripMenuItem,
            this.unosNovogClanaCGToolStripMenuItem});
            this.loyaltiToolStripMenuItem.Name = "loyaltiToolStripMenuItem";
            this.loyaltiToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.loyaltiToolStripMenuItem.Text = "Loyalti";
            // 
            // unosNovogClanaToolStripMenuItem
            // 
            this.unosNovogClanaToolStripMenuItem.Name = "unosNovogClanaToolStripMenuItem";
            this.unosNovogClanaToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.unosNovogClanaToolStripMenuItem.Text = "Unos novog clana";
            this.unosNovogClanaToolStripMenuItem.Click += new System.EventHandler(this.unosNovogClanaToolStripMenuItem_Click);
            // 
            // unosNovogClanaCGToolStripMenuItem
            // 
            this.unosNovogClanaCGToolStripMenuItem.Name = "unosNovogClanaCGToolStripMenuItem";
            this.unosNovogClanaCGToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.unosNovogClanaCGToolStripMenuItem.Text = "Unos novog clana CG";
            this.unosNovogClanaCGToolStripMenuItem.Click += new System.EventHandler(this.unosNovogClanaCGToolStripMenuItem_Click);
            // 
            // racunovodstvoToolStripMenuItem
            // 
            this.racunovodstvoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.osvezavanjeIzvodaToolStripMenuItem});
            this.racunovodstvoToolStripMenuItem.Name = "racunovodstvoToolStripMenuItem";
            this.racunovodstvoToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.racunovodstvoToolStripMenuItem.Text = "Racunovodstvo";
            // 
            // osvezavanjeIzvodaToolStripMenuItem
            // 
            this.osvezavanjeIzvodaToolStripMenuItem.Name = "osvezavanjeIzvodaToolStripMenuItem";
            this.osvezavanjeIzvodaToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.osvezavanjeIzvodaToolStripMenuItem.Text = "Osvezavanje Izvoda";
            this.osvezavanjeIzvodaToolStripMenuItem.Click += new System.EventHandler(this.osvezavanjeIzvodaToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(68, 141);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Nemate konekciju ka serveru";
            this.label3.Visible = false;
            // 
            // btnProveraFakture
            // 
            this.btnProveraFakture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnProveraFakture.Location = new System.Drawing.Point(149, 44);
            this.btnProveraFakture.Margin = new System.Windows.Forms.Padding(2);
            this.btnProveraFakture.Name = "btnProveraFakture";
            this.btnProveraFakture.Size = new System.Drawing.Size(158, 31);
            this.btnProveraFakture.TabIndex = 13;
            this.btnProveraFakture.Text = "Provera Fakture";
            this.btnProveraFakture.UseVisualStyleBackColor = true;
            this.btnProveraFakture.Click += new System.EventHandler(this.btnProveraFakture_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::BebaKids.Properties.Resources.nc11;
            this.pictureBox2.Location = new System.Drawing.Point(36, 141);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(26, 28);
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BebaKids.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(36, 81);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 57);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 238);
            this.Controls.Add(this.btnProveraFakture);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMagacin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnPrijavaPopisa);
            this.Controls.Add(this.btnRealizacijaCekova);
            this.Controls.Add(this.btnPrenosnica);
            this.Controls.Add(this.btnPrijavaOdsustva);
            this.Controls.Add(this.btnPrijavaRadnika);
            this.Controls.Add(this.btnPrijemnica);
            this.Controls.Add(this.btnBarkod);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pocetna";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.formLoad_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnBarkod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrijemnica;
        private System.Windows.Forms.Button btnPrenosnica;
        private System.Windows.Forms.Button btnMagacin;
        private System.Windows.Forms.Button btnPrijavaRadnika;
        private System.Windows.Forms.Button btnPrijavaOdsustva;
        private System.Windows.Forms.Button btnPrijavaPopisa;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnRealizacijaCekova;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem popisObjektaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosPopisaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPrenosToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem radniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajFransizePoDanuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajLageraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proizvodnjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prenosSifaraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalkulacijaArtiklaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dnevniPrometToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosPrometaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledDnevnogIzveštajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loyaltiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosNovogClanaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proveraCeneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosNovogClanaCGToolStripMenuItem;
        private System.Windows.Forms.Button btnProveraFakture;
        private System.Windows.Forms.ToolStripMenuItem racunovodstvoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem osvezavanjeIzvodaToolStripMenuItem;
    }
}

